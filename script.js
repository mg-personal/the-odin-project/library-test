let myLibrary = [];
let node = document.querySelector("#books-div")



function Book(title, author, pages, isRead) {
    this.title = title,
    this.author = author,
    this.pages = pages,
    this.isRead = false,
    this.info = function() {
        console.log(title, author, pages, isRead)
    }
}

function addBookToLibrary() {
    let title = Book.title;
    let author = Book.author;
    let pages = Book.pages;
    let isRead = Book.isRead;
    let book = new Book(title,author,pages,isRead);
    myLibrary.push(book);

}

const TheHobbit = new Book('The Hobbit', 'Mauro', 123, true);
const TheHobbit2 = new Book('The Hobbit2', 'Mauro', 123, true);
const TheHobbit3 = new Book('The Hobbit3', 'Mauro', 123, true);


myLibrary.push(TheHobbit, TheHobbit2, TheHobbit3);

function render() {
    clearNode();
    const booksBody = document.querySelector("#books-div")
    var myUniqueLibrary = myLibrary.filter(function(item, index) {
        return myLibrary.indexOf(item) >= index;
    });
    for (let i = 0; i < myUniqueLibrary.length; i++) {
            const title = document.createElement("h1");
    
            booksBody.appendChild(title);
            title.innerHTML = `${myUniqueLibrary[i].title}`;
        }

    
};


function addBookToMyLibrary(event) {
    const title = document.querySelector("#title").value;
    const author = document.querySelector("#author").value;
    const pages = document.querySelector("#pages").value;

    let book = new Book(title, author, pages);
  
    myLibrary.push(book);
    
  
    render();
    
    event.preventDefault();
  }

function clearNode() {
    node.innerHTML = ""
}


const addBookBtn = document.querySelector("#add-book-btn");
const addBookForm = document.querySelector("#add-book-form");
addBookForm.addEventListener("submit", addBookToMyLibrary)
addBookBtn.addEventListener('click', () => {
    console.log('add new book form button')
    myLibrary.pop();
    render();
});

render();
